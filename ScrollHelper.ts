export class ScrollHelper {

  static scrollToElement(id: string) {
    const el = document.getElementById(id);
    if (el) {
      if (ScrollHelper.isIE()) {
        el.scrollIntoView();
      } else {
        const elementRect = el.getBoundingClientRect();
        const positionFromTop = elementRect.top + document.documentElement.scrollTop - 10;
        window.scroll({
          top: positionFromTop,
          behavior: 'smooth',
        });
      }
    }
  }

  static isIE(): boolean {
    const ua = navigator.userAgent;
    return ua.indexOf('MSIE ') > -1 || ua.indexOf('Trident/') > -1;
  }

  static scrollToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }
}
