export interface PageMetaData {
  title: string;
  description: string;
  ogImage: string;
  ogUrl: string;
}

export class PageHelper {

  static updatePageHead(metaData: PageMetaData) {
    document.title = metaData.title;
    const metaDescription: any = document.head.querySelector('meta[name=description]');
    const ogDescription: any = document.head.querySelector('meta[property=og\\:description]');
    const ogImage: any = document.head.querySelector('meta[property=og\\:image]');
    const ogURL: any = document.head.querySelector('meta[property=og\\:url]');
    const ogTitle: any = document.head.querySelector('meta[property=og\\:title]');

    if (metaDescription && metaDescription.content) {
      metaDescription.content = metaData.description;
    }
    if (ogDescription && ogDescription.content) {
      ogDescription.content = metaData.description;
    }
    if (ogImage && ogImage.content) {
      ogImage.content = metaData.ogImage;
    }
    if (ogURL && ogURL.content) {
      ogURL.content = metaData.ogUrl;
    }

    if (ogTitle && ogTitle.content) {
      ogTitle.content = metaData.title;
    }
  }

}
