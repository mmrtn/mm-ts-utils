export class StorageHelper {

  static isLS: boolean = StorageHelper.islocalStorage();

  static islocalStorage(): boolean {
    const test = 'test';
    try {
      localStorage.setItem(test, test);
      localStorage.removeItem(test);
      console.log('********* localStorage tested successfully!!! *********');
      return true;
    } catch (e) {
      console.error('localStorage unavailable: ' + e);
      return false;
    }
  }

  static save(key: string, value: string) {
    if (key && value && StorageHelper.isLS) {
      localStorage.setItem(key, value);
    }
  }

  static load(key: string): string {
    if (key && StorageHelper.isLS && localStorage.getItem(key) !== null && localStorage.getItem(key) !== 'undefined') {
      return localStorage.getItem(key) || '';
    }
    return '';
  }


  static remove(key: string): void {
    if (StorageHelper.isLS && key) {
      localStorage.removeItem('name');
    }
  }

  static loadJsonObject(key: string): Object | null {
    if (!StorageHelper.isLS) {
      return null;
    }
    const item = localStorage.getItem(key)
    if (item !== null && item !== 'undefined') {
      try {
        const obj: Object = JSON.parse(item) as Object;
        return typeof obj === 'object' ? obj : null;
      } catch (e) {
        return null;
      }
    }
    return null;
  }


  static saveJsonObject(key: string, object: Object) {
    if (StorageHelper.isLS) {
      localStorage.setItem(key, JSON.stringify(object));
    }
  }

}
